import { Component, OnInit, Type, Injectable, Input } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import {TaskService} from './../services/task.service'
import {Task} from './../interfaces/task'
import { Steps } from '../interfaces/steps';
import { Items } from '../interfaces/items';
import { ProductsDetailService } from '../services/products-detail.service';
//import { Options } from '../interfaces/steps';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers: [TaskService]
})

export class ProductsComponent implements OnInit {




  screen = 'Screen1'
  productsSelected: Task = {} as Task;
  stepSelected: Steps = {} as Steps;
  itemsSelected: Items = {} as Items;
  products:Task[]=[];
 
  constructor( private TaskService:  TaskService,
    private productsDetailService: ProductsDetailService) {}

  ngOnInit(){

    this.TaskService.getAllTask().subscribe((res) => {
      this.products = res;
    })

    this.productsDetailService.cancelOrder$.subscribe((_) => {
      this.productsSelected = {} as Task
      this.screen = 'Screen1';
    })

    this.productsDetailService.previusOrder$.subscribe((_) => {

      switch (this.screen) {
          case 'Screen2':
          this.screen = 'Screen2'
          break;
          case 'Screen3':
          this.screen = 'Screen3'
          break;
          case 'Screen4':
            this.screen = 'Screen4'
          break;
        default:
  
          break;
      }
   
    })


  }

  onSelectProduct(index: number){
    this.screen = 'Screen2';
    this.productsSelected = this.products[index];
    this.productsDetailService._productData(this.productsSelected)
  }

  onSelectStep(index: number){
    this.screen = 'Screen3';
    this.stepSelected = this.productsSelected.steps[index];
    this.productsDetailService._stepData(this.stepSelected)
  }


  onSelectItem(index: number){
    this.screen = 'Screen4';
    this.itemsSelected = this.stepSelected.items[index];
    this.productsDetailService._itemData(this.itemsSelected)
  }

}
