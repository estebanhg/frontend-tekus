import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductsDetailService } from '../services/products-detail.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {

  total = 0;
  detailsProducts : any = {};
 

  constructor( public productsDetailService: ProductsDetailService) { }

  ngOnInit(): void {
    this.productsDetailService.productData$.subscribe((res) => {
      this.detailsProducts['product'] = res;
      this.total = res.price
      
    })
    
    this.productsDetailService.stepData$.subscribe((res) => {
      this.detailsProducts['step'] = res;
    })

    this.productsDetailService.itemData$.subscribe((res) => {
      this.detailsProducts['item'] = res;

    })

  }

}
