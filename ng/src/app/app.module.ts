import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { BillComponent } from './bill/bill.component';
import {HttpClientModule} from '@angular/common/http';
import { StoreModule } from '@ngrx/store';

import { ProductsDetailService } from './services/products-detail.service';
import { TaskService } from './services/task.service';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    BillComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({}, {})
  ],
  providers: 
  [ProductsDetailService,
  TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
