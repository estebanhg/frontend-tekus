export interface Items {
    "id": number,
    "name": string,
    "nameStyle": string,
    "description": string,
    "image": string,
    "available": boolean
}