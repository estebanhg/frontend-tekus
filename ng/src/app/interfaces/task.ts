import { Steps } from './steps';

export interface Task {
    "codebar": number,
		"name": string,
		"nameStyle": string,
		"price": number,
		"description": null,
		"image": string,
		"available":boolean,
		"deal": boolean,
		"steps": Steps[]
}
