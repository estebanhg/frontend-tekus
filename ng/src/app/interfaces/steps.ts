import { Items } from './items';

export interface Steps {
		"id": number,
		"name": string,
		"description": string,
		"image": string,
		"selectableItems": number,
		"available": boolean,
		"items": Items[]
}