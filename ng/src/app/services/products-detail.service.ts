import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsDetailService {

  private productData = new BehaviorSubject<any>({});
  productData$ = this.productData.asObservable();
  private stepData = new BehaviorSubject<any>({});
  stepData$ = this.stepData .asObservable();

  private itemData = new BehaviorSubject<any>({});
  itemData$ = this.itemData.asObservable();

  private cancelOrder = new BehaviorSubject<any>({});
  cancelOrder$ = this.cancelOrder.asObservable();

  private previusOrder = new BehaviorSubject<any>({});
  previusOrder$ = this.previusOrder.asObservable();

  constructor() { }

  _productData(data: any) {
    this.productData.next(data);
 
  }

  _stepData (data: any) {
    this.stepData.next(data);
  }

  _itemData(data: any) {

    this.itemData.next(data);
  }

  _cancelOrder (data: any) {
    this.cancelOrder.next(data);
  }

  _previusOrder (data: any) {

    this.previusOrder.next(data);
  }
  
}
