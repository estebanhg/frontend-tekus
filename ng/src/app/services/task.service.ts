import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'; 

import {Task} from './../interfaces/task'
//import { Options } from '../interfaces/steps';

@Injectable({
  providedIn: 'root'

})
export class TaskService {

  
  constructor(
    private http: HttpClient,
    public url: HttpClient
  ) { }

  getAllTask(){
    
    const path = 'http://localhost:4000/products';
    return this.http.get<Task[]>(path);
  
  }

/*   getOptions(){
    //Buscar por Indice 
    const API = 'http://localhost:4000/products';
    
    return this.url.get<Options[]>(API);
    
  } */
}
